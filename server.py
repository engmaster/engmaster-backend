from fastapi import FastAPI
from pydantic import BaseModel
from handlers.words import get_word_list_with_answer


sessions = {} # session storage

app = FastAPI(
    title="EngMaster"
)

class SessionData(BaseModel):
    session_id: int
    api_key: str

@app.post('/v1/sessions/create', response_model=dict)
async def create_session():
    session_id = str(len(sessions) + 1)
    sessions[session_id] = []
    return {'session_id': session_id}

@app.get('/v1/sessions/list', response_model=dict)
async def close_session(session_id: str):
    print("List of sessions...")

@app.get('/v1/sessions/{session_id}/show', response_model=dict)
async def show_session(session_id: int):
    return {'session_id': session_id}

@app.get('/v1/words')
async def get_words():
    return await get_word_list_with_answer("A1", "adjectives", 5)