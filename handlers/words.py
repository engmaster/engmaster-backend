from databases.db import connect_db
from random import randint as rint, shuffle
import json

async def get_wordlist_with_answer(level, kind, count):
    correct_words = await get_correct_words(level, kind)
    incorrect_words = await get_incorrect_words(level, kind, count) 

    data = {
        "Word": correct_words[0][1],
        "Transcription": correct_words[0][2],
        "Translation": correct_words[0][3],
        "Incorrects": incorrect_words
    }

    serialized_data = json.dumps(data, ensure_ascii=False)
 
    return serialized_data
 
async def get_correct_words(level, kind):
    db_connection = connect_db()
    cursor = db_connection.cursor()

    cursor.execute(f'SELECT * FROM words_{level}_{kind} WHERE id = {rint(0, await get_number_of_words(level, kind))}')
    data = cursor.fetchall() # [(5, 'Available', '[əˈveɪləbl]', 'Доступный', None)]

    return data

async def get_number_of_words(level, kind):
    db_connection = connect_db()
    cursor = db_connection.cursor()
    cursor.execute(f'SELECT * FROM words_{level}_{kind}')
    return cursor.rowcount

async def get_incorrect_words(level, kind, count):
    db_connection = connect_db()
    cursor = db_connection.cursor()

    incorrect_words = []
    for i in range(count):
        incorrect_word_id = await get_incorrect_word_id(level, kind, id)
        
        cursor.execute(f'SELECT * FROM words_{level}_{kind} WHERE id = {incorrect_word_id}')
        incorrent_word_translate = cursor.fetchall() # ['Красивый', 'Маленький', 'Удачливый']

        try:
            incorrect_words.append(incorrent_word_translate[0][3])
        except IndexError:
            incorrect_words = await get_incorrect_words(level, kind)
        
    return incorrect_words

async def get_incorrect_word_id(level, kind, id):
    incorrect_id = rint(0, await get_number_of_words(level, kind))
    if incorrect_id == id:
        incorrect_id = await get_incorrect_word_id(level, kind, id)
    
    return incorrect_id